//khai báo thư viện express
const express = require("express");

//khai báo thư viện path
const path = require("path");

//khai báo app express
const app = express();

//khai báo cổng chạy
const port = 8000;

//thêm middleware static để hiển thị ảnh
app.use(express.static(__dirname + "/views"));

app.get('/', (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/Task 44.30.html"));

})
app.get('/cart', (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/cart.html"));

})

app.get('/foods', (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/Task 44.30.html"));

})


//chạy app
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})